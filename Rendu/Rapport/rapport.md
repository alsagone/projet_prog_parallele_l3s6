Hakim ABDOUROIHAMANE - L3 S6 Informatique

# Rapport de projet

### Environnement

Le système utilisé pour réaliser les tests et les mesures de performances est :

```
OS: Linux DESKTOP-CMMLHCT 4.4.0-19041-Microsoft #488-Microsoft Mon Sep 01 13:43:00 PST 2020 x86_64 GNU/Linux
OS dans une machine physique
#processeurs: 16
intel hyper-threading: Thread(s) per core:  2
compilateur: gcc (Debian 8.3.0-6) 8.3.0
```

### Méthode de parallélisation

J'ai décomposé les boucles `for` en plusieurs groupes:

- les régions parallèles contenant une seule boucle `for`, chaque itération nécessitant la même quantité de travail
  J'utilise `#pragma omp parallel for schedule(static)`
  Par exemple:

```
#pragma omp parallel for schedule(static)
    for (int s = 0; s < p->S; s++) {
        ...
    }
```

- Idem mais cette fois-ci la quantité de travail est variable (par exemple à cause d'un `if`)
  J'utilise `#pragma omp parallel for schedule(dynamic)`
  Par exemple:

```
#pragma omp parallel for schedule(dynamic)
    for (int street = 0; street < p->S; street++) {
        // If there is a street to dequeue
        if (street_state[street].out == 1) {
            ...
        }
    }
```

- Les réductions pour les sommes
  J'utilise `#pragma omp parallel for reduction(+ : variable)`
  Par exemple:

```
#pragma omp parallel for reduction(+ : cycle)
    for (int l = 0; l < s->schedule[i].nb; l++) {
        cycle += s->schedule[i].t[l].duree;
    }
```
