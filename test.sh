#!/bin/bash 

#Valeurs par défaut
make=0
filename="e"

#Si -m est dans les arguments
if [[ "$*" == *"-m"* ]]; then 
    make=1

    # S'il y a deux arguments, le fichier est dans un des deux
    if [[ $# -eq 2 ]]; then
        if [[ "$1" == *"-m"* ]]; then 
            filename="$2"
        else 
            filename="$1"
        fi    
    fi

#Sinon s'il n'y a qu'un seul argument, on sait que ce n'est pas un "-m" donc c'est un nom de fichier
elif [[ $# -eq 1 ]]; then 
        filename="$1"
fi

if [[ $make == 1 ]]; then 
    make clean && make 

    if [ $? -ne 0 ]; then
        echo "Erreur make"
        exit 1
    fi
fi

input="input/${filename}.txt"
output="out/${filename}_best.out"

echo "solver ${input} ${output}"
time ./solver "${input}" "${output}"

echo "./checker ${input} ${output}"
time ./checker "${input}" "${output}"

